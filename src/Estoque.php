<?php
namespace AndreLuis\Commerce;

class Estoque
{
    public Produto $produto;
    public int $quantidade;
    public Limite $limite;

    function __construct(Produto $produto, $quantidade, Limite $limite)
    {
        $this->produto = $produto;
        $this->quantidade = $quantidade;
        $this->limite  = $limite;
        
    }

    function consumir($quantidade) {
        if($this->quantidade < $quantidade) {
            throw new \InvalidArgumentException('Estoque Insuficiente');
        };
        $this->quantidade -= $quantidade;
        $this->limite->aviso($this->produto, $this->quantidade);
    }
}

// class TempoLimite {

//     public $tempoEmEstoque;

//     function __construct(int $tempoEmEstoque = 3) {

//         $this->tempoEmEstoque = $tempoEmEstoque;
//     }

//     function estouro(Produto $produto, ) {

//     }
// }