<?php
namespace AndreLuis\Commerce;

class Produto
{
    public $nome;
    public $tipo;

    function __construct(string $nome, int $tipo)
    {
        $this->nome = $nome;
        $this->tipo = $tipo;
    }

    function __toString()
    {
        return $this->nome;
    }
}