<?php
namespace AndreLuis\Commerce;

class Limite
{
    public $valor;

    function __construct(int $valor)
    {
        $this->valor = $valor;
    }


    function aviso(Produto $produto, int $quantidade)
    {
        #Usar o conceito de Listeners para a function aviso.
        
        if ($quantidade <= $this->valor) {
            return "O produto $produto atingiu o limite!";
            //throw new Exception('Produto ultrapassou o limite!');
        }
    }
}