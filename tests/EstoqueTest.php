<?php

use AndreLuis\Commerce\Estoque;
use AndreLuis\Commerce\Limite;
use PHPUnit\Framework\TestCase;
use \AndreLuis\Commerce\Produto;

final class EstoqueTest extends TestCase{
    public function testEstoque() {
        $estoque = new Estoque(new Produto("Pão de Mel", 3),50,new Limite(5));
        $estoque->consumir(50);
    }

    public function testException(){
        $this->expectException(InvalidArgumentException::class);

        $estoque = new Estoque(new Produto("Arroz", 4), 100, new Limite(10));
        $estoque->consumir(101);
    }
}