<?php

// $number = 1;
// echo $number * 3, "\n";

// $textNumber = '1';
// echo $textNumber * 3, "\n";

// $text = 'Hello World';
// echo $text * 3;

// $nome = 'Fulano';

// $ponteiroParaNome = &$nome;

// $ponteiroParaNome .= ' Ciclano';

// var_dump($nome);
// var_dump($ponteiroParaNome);

// $array = [];
// var_dump($array);

// function sub($a, $b) {
//     return $a - $b;
// }

// function soma($a, $b) {
//     return $a + $b;
// }

// # $fn = sub;
// $fn = 'sub';

// echo $fn(4 , 5) , "\n";

// $fn = 'soma';

// echo $fn(2, 2), "\n";

// $name = 'World';

// $helloWorld = function($greetings) use ($name) {
//     echo $greetings, ' ', $name, '!!!';
// };

// $helloWorld('Hello');

// $item = 1;

// $result_match = match (True) {
//     $item >= 2 => 'Farinha',
//     $item >= 3 => 'Arroz',
//     $item >= 4 => 'Milho',
//     default => 'Oléo',
// };

// function id($result_match){
//     if($result_match = 1){
//         $first_option = md5(uniqid(rand(), true));
//     }elseif($result_match = 2){
//         $second_option = md5(uniqid(rand(), true));
//     }elseif($result_match = 3){
//         $third_option = md5(uniqid(rand(), true));
//     }else{
//         $fourth_option = md5(uniqid(rand(), true));
//     }
// };

// class Pessoa {
//     public $nome;

//     function __construct(string $nome) {
//         $this->nome = $nome;
//     }

//     function __toString() : string {
//         return "Pessoa {nome: $this->nome}";
//     }
// }

// $pessoa0 = new Pessoa('André');

// echo $pessoa0, "\n";

class Produto
{
    public $nome;
    public $tipo;

    function __construct(string $nome, int $tipo)
    {
        $this->nome = $nome;
        $this->tipo = $tipo;
        return "Produto: {nome: $this->nome} Cadastrado.";
    }
}

class Estoque
{
    public Produto $produto;
    public int $quantidade;
    public Limite $limite;

    function __construct(Produto $produto, $quantidade, Limite $limite)
    {
        $this->produto = $produto;
        $this->quantidade = $quantidade;
        $this->limite  = $limite;
        
    }

    function consumir($quantidade) {
        if($this->quantidade <= $quantidade) {
            throw new Exception('');
        };
        $this->quantidade -= $quantidade;
        $this->limite->aviso($this->produto, $this->quantidade);
    }
}

class Limite
{
    public $valor;

    function __construct(int $valor = 5)
    {
        $this->valor = $valor;
    }


    function aviso(Produto $produto, int $quantidade)
    {
        if ($quantidade <= $this->valor) {
            return "O produto {$this->produto} atingiu o limite!";
            //throw new Exception('Produto ultrapassou o limite!');
        }
    }
}

class TempoLimite {

    public $tempoEmEstoque;

    function __construct(int $tempoEmEstoque = 3) {

        $this->tempoEmEstoque = $tempoEmEstoque;
    }

    function estouro(Produto $produto, ) {

    }
}


// $leite = new Produto('Leite', 1);
// $limite = new Limite(4);
// $estoqueDeLeite = new Estoque($leite, 10, $limite);
// $estoqueDeLeite->consumir(5);
// $estoqueDeLeite->consumir(2);